package org.example.arrays;

import java.util.Arrays;

public class ArrayImplementations {

    public static void main(String[] args) {

        int [] arr1 = new int[]{1,2,3,4,5,6,7,8,9};
        Arrays.stream(arr1).forEach(System.out::print);

        System.out.println(args.length);
    }
}
