package org.example.arrays;

public class FindRepeatingNumbers {

    public static void findRepeatingNumbers(int[] arr) {
        System.out.print("Repeating numbers are: ");

        for (int i = 0; i < arr.length; i++) {
            int index = Math.abs(arr[i]) - 1;

            if (arr[index] < 0) {
                // The absolute value of arr[i] has already been encountered
                System.out.print(index + 1 + " ");
            } else {
                // Mark the element as visited by negating its value
                arr[index] = -arr[index];
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 10, 5};
        findRepeatingNumbers(arr);
    }
}
