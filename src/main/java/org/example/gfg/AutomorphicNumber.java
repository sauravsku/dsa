package org.example.gfg;

public class AutomorphicNumber {

    public static void main(String[] args) {

        int n = 540;
        int mod1,mod2;
        mod1=n%10;
        int square=n*n;
        mod2=square%10;
        if(mod1==mod2){
            System.out.println("Automorphic");
        } else
             System.out.println("Not Automorphic");
    }
}
