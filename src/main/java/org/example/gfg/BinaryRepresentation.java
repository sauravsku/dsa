package org.example.gfg;

public class BinaryRepresentation {

    public static void main(String[] args) {

        int N = 20;
        StringBuilder sb = new StringBuilder();
        while(N!=1) {
            int rem = N%2;
            sb.append(rem);
            N/=2;
            if(N==1) sb.append(1);
        }
        int len = sb.length();
        if(len!=30) {
            int diff = 30-len;
            while(sb.length()!=30) {
                sb.append(0);
            }
        }

        System.out.println(sb.reverse().toString());

    }
}
