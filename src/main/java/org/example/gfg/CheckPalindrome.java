package org.example.gfg;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CheckPalindrome {

    private static boolean checkPalindrome(int n) {

//        return Stream.of(String.valueOf(n))
//                .map(x->new StringBuilder(x).reverse())
//                .collect(Collectors.joining("")).equals(String.valueOf(n));
        String s1 = String.valueOf(n);
        String s2 = new StringBuilder(s1).reverse().toString();

        return s1.equals(s2);
    }

    public static int palinArray(int[] a, int n)
    {
        //add code here.
        int palinStatus = 0;
        for(int i=0; i<a.length; i++) {
            if(checkPalindrome(a[i]))
                palinStatus = 1;
            else
                palinStatus = 0;
        }

        return palinStatus;
    }
    public static void main(String[] args) {
        int[] ar = {111, 121, 1221};
        System.out.println(palinArray(ar, 3));
    }
}
