package org.example.gfg;

public class Complement1 {

    public static void main(String[] args) {

        String S ="010";
        char [] ch = S.toCharArray();
        for(int i=0;i<ch.length;i++) {
            if(ch[i]=='1')
                ch[i]='0';
            else
                ch[i]='1';
        }
        System.out.println(new String(ch));
    }
}
