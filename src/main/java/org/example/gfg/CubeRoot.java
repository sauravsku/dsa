package org.example.gfg;

public class CubeRoot {

    public static void main(String[] args) {

        int n = 17;
        int cubeRoot = (int) Math.pow(n,0.33333333);
        System.out.println(cubeRoot);
    }
}
