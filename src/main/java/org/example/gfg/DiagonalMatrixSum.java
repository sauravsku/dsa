package org.example.gfg;

public class DiagonalMatrixSum {
    public static void main(String[] args) {

        int [][] matrix = {{1, 2}, {3, 4}};
        int sum = 0;
        int i =0;
        int len = matrix[0].length;
        while(i<len) {
            sum+=matrix[i][i];
            i++;
        }

        int j =0;
        int r = len-1;
        while(j<len) {
            sum += matrix[j][r];
            j++;
            r--;
        }
        System.out.println(sum);
    }
}
