package org.example.gfg;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DisplayLargestNameSize {

    public static void main(String[] args) {

        String names[] = { "Geek", "Geeks", "Geeksfor", "GeeksforGeek", "GeeksforGeeks" };
        String s = Arrays.asList(names).stream().max(Comparator.comparingInt(String::length)).get();

        System.out.println(s);

    }
}
