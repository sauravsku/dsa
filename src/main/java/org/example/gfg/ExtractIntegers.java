package org.example.gfg;

import java.util.ArrayList;

public class ExtractIntegers {

    public static void main(String[] args) {


        String s = "1: Prakhar Agrawal, 2: Manish Kumar Rai, \n" +
                "     3: Rishabh Gupta56";
        StringBuilder sb = new StringBuilder();
        for(int i = 0 ; i < s.length() ; i++) {
            char c = s.charAt(i);
            if(Character.isDigit(c)) {
                sb.append(c);
                while (i + 1 < s.length() && Character.isDigit(s.charAt(i + 1))) {
                    sb.append(s.charAt(++i));
                }
                sb.setLength(0);
            }
        }
        System.out.println(sb.toString());
    }
}
