package org.example.gfg;

public class Fascinating {

    public static void main(String[] args) {

        int n = 192;
        StringBuilder sb = new StringBuilder();
        sb.append(Long.toString(n));
        sb.append(Long.toString(n*2));
        sb.append(Long.toString(n*3));

        char [] ch = sb.toString().toCharArray();

        int [] res = {0,0,0,0,0,0,0,0,0};
        for(char c : ch ) {
            int no = Integer.parseInt(Character.toString(c));
            if(no>0) {
                res[no-1] = res[no-1] + 1;
            }
        }

        boolean status = false;
        for(int j=0;j<9;j++) {
            if(res[j]>1 || res[j]==0){
                status = false;
                break;
            } else {
                status = true;
            }
        }
        System.out.println(status);
    }
}
