package org.example.gfg;

public class GCD2 { //GREATEST COMMON DIVISOR.

    public static int gcd(int a, int b) {

        int min =Math.min(a,b);

        while(min>0) {
            if(a%min==0 && b%min ==0) {
                break;
            }
            min--;
        }
        return min;
    }

    private static int recursiveGcd(int a, int b) {

        if(a==0)
            return b;
        return gcd(b%a, a);
    }

    public int gcdLessTime(int a , int b) {

        while(b!=0){
            int temp = b;
            b = a%b;
            a=temp;
        }
        return a;
    }

    public static void main(String[] args) {

        int a = 8;
        int b = 9;
        System.out.println(gcd(a,b));
    }
}
