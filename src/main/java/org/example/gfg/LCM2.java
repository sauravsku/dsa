package org.example.gfg;

import java.util.Map;

public class LCM2 { //SMALLEST NUMBER THAT DIVIDES BOTH THE NUMBER.

    public static int lcm(int a, int b) {

        return (a/gcd(a,b)) * b;
    }

    private static int gcd(int a, int b) {

        int min = Math.min(a,b);
        while(min>0) {
            if(a%min==0 && b%min==0)
                break;
            min--;
        }
        return min;
    }
    public static void main(String[] args) {

        int a =6, b=18;
        System.out.println(lcm(a,b));
    }
}
