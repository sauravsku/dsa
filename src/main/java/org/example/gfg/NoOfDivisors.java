package org.example.gfg;

public class NoOfDivisors {

    public static void main(String[] args) {

        long N = 6;
        long ans =0;
        for(int i =1; i<=Math.sqrt(N); i++) {
            if(N%i==0) {
                if(i%3==0)
                    ans++;
                if(i != N / i && (N / i) % 3 == 0)
                    ans++;
            }

        }
        System.out.println(ans);
    }
}
