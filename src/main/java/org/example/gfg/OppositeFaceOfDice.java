package org.example.gfg;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class OppositeFaceOfDice {

    public static void main(String[] args) {

        Map<Integer, Integer> mp = new HashMap<>();
        mp.put(1,6);
        mp.put(2,5);
        mp.put(3,4);

        int ans=0;
        int n = 5;
       for(Map.Entry<Integer, Integer> e : mp.entrySet()){
           if(!e.getKey().equals(n)){
               if(e.getValue().equals(n))
                   ans = e.getKey();
           } else {
               ans = e.getValue();
           }
       }
        System.out.println(ans);
    }
}
