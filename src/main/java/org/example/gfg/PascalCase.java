package org.example.gfg;

public class PascalCase {

    public static void main(String[] args) {

        String input = "geeks for geeks";
        char [] ch = input.toCharArray();

        String [] str = input.split(" ");
        StringBuilder sb = new StringBuilder();
        for(String x: str) {
            sb.append(x.substring(0,1).toUpperCase()).append(x.substring(1)).append(" ");
        }
        System.out.println(sb.toString().trim());
    }
}
