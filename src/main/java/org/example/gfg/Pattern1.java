package org.example.gfg;

public class Pattern1 {

    public static void main(String[] args) {
        char ch = '1';
        String s = String.valueOf(ch);
        int n=2;
        for(int i=n; i>0; i--) {
            int elemPerRow = i*n;
            int factor = elemPerRow/n;
            int start=n;
            int currentCount = 0;
            for(int j=0; j<elemPerRow; j++) {
               if(factor==currentCount) {
                   start--;
                   currentCount = 0;
               }
               System.out.print(start+" ");
               currentCount++;
            }
            System.out.println("\n");
        }
    }
}
