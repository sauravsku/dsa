package org.example.gfg;

import java.util.Stack;

public class PerfectArrays {

    public static void main(String[] args) {

        int a[] = {15, 20, 5, 6, 5, 6, 13, 4, 3, 4, 13, 6, 5, 6, 5, 20, 15};

        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < 17; i++) {
            stack.push(a[i]);
        }
        boolean res = false;
        for (int j = 0; j < a.length; j++) {
            if (stack.pop() == a[j])
                res = true;
            else {
                res = false;
                break;
            }

        }
        System.out.println(res);
    }
}
