package org.example.gfg;

public class PerfectNumber {


    static int fact(int n) {
        if(n==1 || n==0) return 1;
        return n*fact(n-1);
    }
    static int isPerfect(int N) {
        // code here
        int sum = 0;
        while(N>0) {
            int rem = N%10;
            sum += fact(rem);
            N/=10;
        }
        if(sum==N)
            return 1;
        return 0;
    }
    public static void main(String[] args) {

        System.out.println(isPerfect(1000000000));

    }
}
