package org.example.gfg;

public class PrimeNumber {

    static boolean isPrime(int n) {
        if(n<=1) return false;
        for(int i=2; i<=Math.sqrt(n);i++) {
            if(n%i==0) {
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args) {

        int n = 275;
        boolean  status = isPrime(n);

        while(n>0) {
            int rem = n%10;
//            if(rem==0 || rem==1 ||rem==4 ||rem==6 ||rem==8 ||rem==9) {
//                status = false;
//                break;
//            }
            if(!isPrime(rem)) {
                status=false;
                break;
            }
            n/=10;
        }

        if(status)
            System.out.println("Prime No");
        else
            System.out.println("Not Prime");
    }
}
