package org.example.gfg;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ProductOfElements {

    public static void main(String[] args) {

        int [] arr = {1,1,2, 2, 3, 4, 5};

        int pro = Arrays.stream(arr).reduce(1,(x,y)->x*y);
        int p = IntStream.of(arr).reduce(1,(x,y)->x*y);
        System.out.println(p);
    }
}
