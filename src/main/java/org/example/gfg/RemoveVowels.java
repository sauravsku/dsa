package org.example.gfg;

public class RemoveVowels {

    public static void main(String[] args) {

        String S = "geeks for geeks";
        StringBuilder sb = new StringBuilder();
        char[] ch = S.toCharArray();
        String check = "aeiou";
        for(int i=0; i<ch.length; i++) {
            if(!check.contains(Character.toString(ch[i]))) {
                sb.append(ch[i]);
            }
        }
        System.out.println(sb.toString());
    }
}
