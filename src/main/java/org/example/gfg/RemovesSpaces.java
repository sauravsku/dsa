package org.example.gfg;

import java.util.stream.Collectors;

public class RemovesSpaces {

    public static void main(String[] args) {

        String str = "  asd asd ";
        String res = str.trim().chars().
                filter(x->!Character.isWhitespace(x)).
                mapToObj(x->String.valueOf((char) x)).collect(Collectors.joining());
        System.out.println(res);
    }
}
