package org.example.gfg;

public class ReversingVowels {
    public static void main(String[] args) {

        String s = "practice";
        String vo = "aeiou";
        char [] ch = vo.toCharArray();
        int n = s.length();

        char [] str = s.toCharArray();

        int i=0;
        int j=n-1;
        boolean swap1 = false;
        boolean swap2 = false;
        while(i<n && j>=0 && i<j) {

            if(!vo.contains(Character.toString(str[i]))) {
                i++;
                swap1 = false;
            }
            else {
                swap1 = true;
            }
            if(!vo.contains(Character.toString(str[j]))) {
                j--;
                swap2 = false;
            }
            else {
                swap2 = true;
            }

            if (swap1 == true && swap2 == true) {
                char temp = str[i];
                str[i] = str[j];
                str[j] = temp;
                i++;
                j--;
            }
        }
        System.out.println(str.toString());
    }
}
