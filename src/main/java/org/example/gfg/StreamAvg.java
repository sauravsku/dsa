package org.example.gfg;

public class StreamAvg {

    public static void main(String[] args) {

        int [] arr = {39, 72, 44, 66, 57, 70, 63, 91, 70, 77, 12, 80,
                56, 10, 80, 72, 37, 88, 73, 84, 61, 41, 57, 26, 37, 7};
        double [] avgs = new double[26];

        for(int i=0; i<arr.length; i++) {
            int sum = 0;
            int j=i;
            while(j>=0) {
                sum+=arr[j];
                j--;
            }
            avgs[i] = (float)sum/(float)(i+1);
        }
        System.out.println(avgs);
    }
}
