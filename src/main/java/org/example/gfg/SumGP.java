package org.example.gfg;

public class SumGP {
    public static void main(String[] args) {

        int a = 1;
        int n = 2;
        int r = 1;
        long sum = r==1 ? n*a : (long)(a*(Math.pow(r,n)-1))/(r-1);
        System.out.println(sum);
    }
}
