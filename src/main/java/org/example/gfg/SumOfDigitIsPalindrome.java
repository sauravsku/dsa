package org.example.gfg;

public class SumOfDigitIsPalindrome {

    static boolean isPalindrome(int n) {

        String s1 = Integer.toString(n);
        String s2 = new StringBuilder(Integer.toString(n)).reverse().toString();
        return s1.equals(s2);
    }
    static int isDigitSumPalindrome(int N) {
        // code here

        int sum = 0;
        while(N>0){
            int r = N%10;
            sum += r;
            N = N/10;
        }

        return isPalindrome(sum) ? 1 : 0;
    }
    public static void main(String[] args) {

        System.out.println(isDigitSumPalindrome(56));
    }
}
