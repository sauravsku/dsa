package org.example.gfg;

public class SumOfSquareEvenNos {

    public static void main(String[] args) {

        int n =2;
        int i = 2;
        int count = 0;
        long sum =0;
        while(count<n) {

            if(i%2==0) {
                sum += i*i;
                count++;
            }
            i++;
        }
        System.out.println(sum);
    }
}
