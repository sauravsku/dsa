package org.example.gfg;

public class SumPalindrome {

    static boolean isPalin(long n) {

        String s1 = Long.toString(n);
        String s2 = new StringBuilder(Long.toString(n)).reverse().toString();
        return s1.equals(s2);
    }
    static long isSumPalindrome(long n){
        // code here
        if(isPalin(n)) return n;


        long ans=0;
        int i=0;
        while(!isPalin(n)) {
            long newN = Long.parseLong(new StringBuilder(Long.toString(n)).reverse().toString());
            n +=newN;
            ans = n;
            i++;
        }
        return ans;

    }

    public static void main(String[] args) {
        System.out.println(isSumPalindrome(4762));
    }
}
