package org.example.gfg;

import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void main(String[] args) {

        int N = 5;
        int a[] = {1,2,3,5,5};
        int key = 5;
        List<Integer> res = new ArrayList<>();
        for(int i=0; i<N; i++){
            if(a[i]==key)
                res.add(i);
        }

        int arr = res.stream().mapToInt(Integer::intValue).toArray().length;

            System.out.println(arr);
    }
}
