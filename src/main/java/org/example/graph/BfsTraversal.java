package org.example.graph;

import java.util.*;

public class BfsTraversal {

    public static void main(String[] args) {

        int N = 9;
        ArrayList<List<Integer>> adjList = new ArrayList<>();
        List<Integer> n1 = Arrays.asList(1,2,3,4);
        List<Integer> n2 = Arrays.asList(2,5);
        List<Integer> n3 = Arrays.asList(3,2,7);
        List<Integer> n4 = Arrays.asList(4,3,7);
        List<Integer> n5 = Arrays.asList(5, 3,6);
        List<Integer> n6 = Arrays.asList(6,3,8);
        List<Integer> n7 = Arrays.asList(7,6,8,9);
        List<Integer> n8 = Arrays.asList(8,9);
        List<Integer> n9 = Arrays.asList(9,6);

        adjList.add(n1);
        adjList.add(n2);
        adjList.add(n3);
        adjList.add(n4);
        adjList.add(n5);
        adjList.add(n6);
        adjList.add(n7);
        adjList.add(n8);
        adjList.add(n9);

        System.out.println(bfsTraversal(adjList, N));


    }

    private static ArrayList<Integer> bfsTraversal(ArrayList<List<Integer>> adjList, int n) {

        boolean [] visited = new boolean[n];
        Queue<Integer> queue = new LinkedList<>();
        ArrayList<Integer> bfs = new ArrayList<>();
        queue.add(0);
        visited[0] = true;

        while(!queue.isEmpty()) {

            Integer node = queue.poll(); //returns index for first element in queue.
            bfs.add(node);

            for(Integer it : adjList.get(node)) {
                if(!visited[it-1]) {
                    visited[it-1] = true;
                    queue.add(it-1);
                }
            }
        }
        return bfs;
    }
}
