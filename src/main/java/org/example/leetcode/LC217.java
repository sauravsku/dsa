package org.example.leetcode;

import java.util.HashMap;
import java.util.Map;

public class LC217 {

    public static boolean containsDuplicate(int[] nums) {

        //Using hashing method--faster
        int size = 4;
        while(size < nums.length) size <<= 1;
        int[] hashtable = new int[size];
        boolean zero = false;
        int formula = 123123;
        for(int i = 0; i < nums.length; i++) {
            if(nums[i] == 0) {
                if(!zero) {
                    zero = true;
                } else {
                    return true;
                }
            }
            int hash = nums[i] * 12123213;
            while(true) {
                int index = hash++ & (size - 1);
                int val = hashtable[index];
                if(val == 0) {
                    hashtable[index] = nums[i];
                    break;
                } if(val == nums[i]) return true;
            }
        }
        return false;

        /*
        * class Solution {
    public boolean containsDuplicate(int[] nums) {
        for(int i=0; i<nums.length; i++) {
            int current = nums[i];
            int j = i-1;

            while(j>=0 && current<nums[j]) {
                nums[j+1] = nums[j];
                j--;
            }

            if(j>=0 && current==nums[j]) return true;

            nums[j+1] = current;
        }

        return false;
    }
}
        * */

      /*  Map<Integer, Integer> mp = new HashMap<>();

        boolean duplicates = false;
        for(int no : nums) {
            if(!mp.containsKey(no)) {
                mp.put(no, 1);
            } else {
                mp.put(no, mp.getOrDefault(no, 0)+1);
                duplicates = true;
                break;
            }
        }
        return duplicates;*/
    }
    public static void main(String[] args) {

        int [] nums = {1,2,3,4,5,6,7,8,8,8,6};
        System.out.println(containsDuplicate(nums));
    }
}
