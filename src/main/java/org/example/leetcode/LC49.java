package org.example.leetcode;

import java.util.*;

public class LC49 {

    private static List<List<String>> groupAnagrams(String [] strs) {
        Map<String, List<String> > map = new HashMap<>();

        for(String word: strs){
            char[] ch = word.toCharArray();
            Arrays.sort(ch);
            String str = new String(ch);

            if(!map.containsKey(str)){
                map.put(str, new ArrayList<>());
            }

            map.get(str).add(word);
        }
        return new ArrayList<>(map.values());
    }

    public static void main(String[] args) {

        String [] strs = {"eat","tea","tan","ate","nat","bat"};

        System.out.println(groupAnagrams(strs));;
    }
}
