package org.example.sorting;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class BubbleSort {

    public static void main(String[] args) {

        int [] ar = {5,4,3,2,1};

        bubbleSort(ar, ar.length);
        for (int x : ar) {
            System.out.println(x+" ");
        }
    }

    private static void bubbleSort(int[] ar, int length) {

        int inner, outer;
        boolean swapped;
        for(outer = 0; outer < length-1; outer++) {
            swapped = false;
            for(inner = 0; inner < length-outer-1; inner++) {
                if(ar[inner] > ar[inner + 1]) {
                    int temp = ar[inner];
                    ar[inner] = ar[inner + 1];
                    ar[inner + 1] = temp;
                    swapped = true;
                }
            }
            if (swapped == false)
                break;
        }
    }
}
