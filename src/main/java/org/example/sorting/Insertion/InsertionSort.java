package org.example.sorting.Insertion;

import java.util.Arrays;

/**
 * Used for smaller input size. Useful when input size is almost sorted.
 *
 */
public class InsertionSort { //Stable o(n^2) algorithm.

    public static Integer [] insertionSort(Integer [] arr, int n) {

        for (int i=1; i<n; i++) {
            int key = arr[i];
            int j = i-1;
            while(j>=0 && arr[j]>key){
                arr[j+1] = arr[j];
                j -=1;
            }
            arr[j+1] = key;
        }
        return arr;
    }

    public static void main(String[] args) {
        Integer [] arr = {9,8,7,6,5,4,3,2,1};
        Integer [] sortedArray = insertionSort(arr, arr.length);
        Arrays.asList(sortedArray).stream().forEach(x->System.out.print(x+" "));
    }
}
