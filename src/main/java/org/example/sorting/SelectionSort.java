package org.example.sorting;

import java.util.Arrays;

public class SelectionSort {

    private static Integer [] selectionSortAlgorithm(Integer []arr, int n) {
        for(int i=0; i<n; i++) {
            int min_idx = i;
            for(int j=i+1; j<n; j++) {
                if(arr[j]<arr[min_idx])
                    min_idx = j; //Finding minimum elements

            }
            int temp = arr[min_idx];
            arr[min_idx] = arr[i];
            arr[i] = temp;
        }
        return arr;
    }
    public static void main(String[] args) {
        Integer []arr = {9,8,7,6,5,4,3,2,1};
        Integer [] sortedArray = selectionSortAlgorithm(arr, arr.length);
        Arrays.asList(sortedArray).stream().forEach(x->System.out.print(x+" "));
    }
}
