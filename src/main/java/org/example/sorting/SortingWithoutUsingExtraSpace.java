package org.example.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SortingWithoutUsingExtraSpace {
    public static void main(String[] args) {

        Integer [] arr = {0,1,0,0,0,1,1,1,0,5,-1};

        for(int i=0;i<arr.length;i++){
            for(int j=i+1;j<arr.length;j++) {
                if(arr[i]>arr[j]){
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        Arrays.asList(arr).stream().forEach(x->System.out.print(x));


    }
}
