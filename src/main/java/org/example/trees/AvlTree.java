package org.example.trees;

class Node {
    int data;
    Node left, right;
    int height;

    Node(int d) {
        data = d;
        height = 1;
    }
}

public class AvlTree {

    Node root;

    Node insert(Node root, int value) {

        if (root == null)
            return (new Node(value));

        // Normal BST Insertion.
        if (value < root.data)
            root.left = insert(root.left, value);
        else if (value > root.data)
            root.right = insert(root.right, value);
        else  //Duplicate node is not allowed.
            return root;

        //Update height of this ancestor node.
        root.height = 1 + findMax(findHeight(root.left), findHeight(root.right));

        //Get the balance factor of this ancestor node to check whether this node became unbalanced .
        int balanceFactor = checkBalanceFactor(root);

        // If this node becomes unbalanced, then there are 4 cases.

        // 1. Left-Left Case
        if (balanceFactor > 1 && value < root.left.data)
            return rotateRight(root);

        // 2. Right-Right Case
        if (balanceFactor < -1 && value > root.right.data)
            return rotateLeft(root);

        // 3. Left-Right Case
        if (balanceFactor > 1 && value > root.left.data) {
            root.left = rotateLeft(root.left);
            return rotateRight(root);
        }


        // 4. Right-Left Case
        if (balanceFactor < -1 && value < root.right.data) {
            root.right = rotateRight(root.right);
            return rotateLeft(root);
        }

        return root;
    }

    //Utility function to find the height of the tree.
    int findHeight(Node node) {

        if (node == null)
            return 0;
        return node.height;
    }

    int checkBalanceFactor(Node node) {

        if (node == null)
            return 0;

        return findHeight(node.left) - findHeight(node.right);
    }

    Node rotateRight(Node x) {

        Node newNode = x.left;
        Node t2 = newNode.right;

        //Perform rotation.
        newNode.right = x;
        x.left = t2;

        //update heights
        x.height = 1 + findMax(findHeight(x.left), findHeight(x.right));
        newNode.height = 1 + findMax(findHeight(newNode.left), findHeight(newNode.right));

        return newNode;
    }

    Node rotateLeft(Node x) {

        Node newNode = x.right;
        Node t2 = newNode.left;

        //Perform rotation.
        newNode.left = x;
        x.right = t2;

        //update heights
        x.height = 1 + findMax(findHeight(x.left), findHeight(x.right));
        newNode.height = 1 + findMax(findHeight(newNode.left), findHeight(newNode.right));

        //return new node.
        return newNode;
    }

    void preOrder(Node root) {

        if (root != null) {
            System.out.println(root.data + " ");
            preOrder(root.left);
            preOrder(root.right);
        }
    }

    int findMax(int a, int b) {
        return Math.max(a, b);
    }

    public static void main(String[] args) {

        AvlTree avlTree = new AvlTree();
        avlTree.root = avlTree.insert(avlTree.root, 10);
        avlTree.root = avlTree.insert(avlTree.root, 20);
        avlTree.root = avlTree.insert(avlTree.root, 30);
        avlTree.root = avlTree.insert(avlTree.root, 40);
        avlTree.root = avlTree.insert(avlTree.root, 50);
        avlTree.root = avlTree.insert(avlTree.root, 50);
        avlTree.root = avlTree.insert(avlTree.root, 25);

        avlTree.preOrder(avlTree.root);
    }
}



