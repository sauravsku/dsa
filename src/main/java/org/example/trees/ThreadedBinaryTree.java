package org.example.trees;



public class ThreadedBinaryTree {

    static class Node {
        int data;
        Node left, right;
        boolean isThreaded;

    }

    static Node createThreadedTree(Node root) {

        //Tree is empty or single node.
        if(root == null)
            return null;
        if(root.left == null && root.right == null)
            return root;

        //Find predecessor if it exists (rightmost child in left subtree)
        if(root.left != null) {

            Node left = createThreadedTree(root.left);

            //Link a thread from predecessor to root.
            left.right = root;
            left.isThreaded  = true;
        }

        //If currentNode is rightmost child.
        if (root.right == null)
            return root;

        return createThreadedTree(root.right);
    }

    static Node leftMost(Node root) {

        while(root != null && root.left !=null)
            root = root.left;
        return root;
    }

    static void inOrderTraversal(Node root) {

        if(root == null) return;

        //Find the leftmost node in Binary Tree.
        Node cur = leftMost(root);

        while(cur!=null) {

            System.out.print(cur.data + "->");

            //If this node is a thread node then go to its inorder successor.
            if(cur.isThreaded)
                cur = cur.right;
            else
                cur = leftMost(cur.right);
        }
    }
    static Node createNode(int value) {

        Node temp  = new Node();
        temp.data = value;
        temp.left = temp.right = null;
        return temp;
    }

    public static void main(String[] args) {

        Node root = createNode(1);
        root.left = createNode(2);
        root.right = createNode(3);
        root.left.left = createNode(4);
        root.left.right = createNode(5);
        root.right.left = createNode(6);
        root.right.right = createNode(7);

        createThreadedTree(root);

        inOrderTraversal(root);
    }
}
